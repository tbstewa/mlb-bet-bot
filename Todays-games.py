#Import necessary libraries
import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
import csv

#Create a class to get the games for today
class TodaysGames:
    #Create a function to get the games for today
    def get_games(self):
        #Create a variable to hold the url
        url = 'http://www.espn.com/mlb/schedule'
        #Create a variable to hold the response from the url
        response = requests.get(url)
        #Create a variable to hold the html from the response
        html = response.content
        #Create a variable to hold the soup
        soup = BeautifulSoup(html, 'html.parser')
        #Create a variable to hold the games for today
        games = soup.find_all('div', class_='score')
        #Create a variable to hold the games for today
        games = [game.text for game in games]
        #Create a variable to hold the games for today
        games = [game.replace(' ', '') for game in games]

#Create a function to get a list list of matchups for today with home and away teams
    def get_matchups(games):
        matchups = []
#Create a variable to hold the index
        index = 0
#Create a for loop to iterate through the games
        for game in games:
    #Create a variable to hold the matchup
            matchup = games[index] + ' ' + games[index + 1]
    #Append the matchup to the matchups list
            matchups.append(matchup)
    #Increment the index by 2
            index += 2
#Create a variable to hold the matchups
        matchups = pd.DataFrame(matchups)
#Create a variable to hold the matchups
        matchups = np.array(matchups)
#Return the matchups
        return matchups

#Create a class to get the outcomes for today's games after the games have been played using requests and BeautifulSoup and the espn.com website's scoreboard and store the outcomes in a csv file called outcomes.csv
class TodaysOutcomes:
    #Create a function to get the outcomes for today's games
    def get_outcomes(self):
        #Create a variable to hold the url
        url = 'http://www.espn.com/mlb/scoreboard'
        #Create a variable to hold the response from the url
        response = requests.get(url)
        #Create a variable to hold the html from the response
        html = response.content
        #Create a variable to hold the soup
        soup = BeautifulSoup(html, 'html.parser')
        #Create a variable to hold the outcomes for today's games
        outcomes = soup.find_all('div', class_='score')
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.text for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace(' ', '') for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace('Final', '') for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace('F/10', '') for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace('F/9', '') for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace('F/8', '') for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace('F/7', '') for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace('F/6', '') for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace('F/5', '') for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace('F/4', '') for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace('F/3', '') for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace('F/2', '') for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace('F/1', '') for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace('F/0', '') for outcome in outcomes]
        #Create a variable to hold the outcomes for today's games
        outcomes = [outcome.replace('F', '') for outcome in outcomes]
        
        #Create a variable to hold the outcomes for today's games
        outcomes = pd.DataFrame(outcomes)
        #Create a variable to hold the outcomes for today's games
        outcomes = np.array(outcomes)
        #Return the outcomes for today's games
        return outcomes
    #Store the outcomes for today's games in a csv file called outcomes.csv
    def store_outcomes(self):
        #Create a variable to hold the outcomes for today's games
        outcomes = self.get_outcomes()
        #Create a variable to hold the csv file
        csv_file = csv.writer(open('outcomes.csv', 'w'))
        #Create a variable to hold the header row
        header_row = ['Visitor Runs', 'Home Runs']
        #Write the header row to the csv file
        csv_file.writerow(header_row)
        #Create a for loop to iterate through the outcomes for today's games
        for outcome in outcomes:
            #Write the outcome to the csv file
            csv_file.writerow(outcome)
