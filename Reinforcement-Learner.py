#Import the necessary libraries
import csv
import numpy as np
import pandas as pd
import pychadwick
import random
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental import preprocessing
#Create a class to hold the Reinforcement Learner
class ReinforcementLearner:
    #Create a function to retrieve the data from the csv file created by Retreive-Data.py
    def retrieve_data(self):
        #Create a variable to hold the csv file
        csv_file = csv.writer(open('GameData.csv', 'r'))
        #Create a variable to hold the header row
        header_row = ['Date', 'Visitor', 'Visitor Runs', 'Home', 'Home Runs', 'Visitor Hits', 'Home Hits', 'Visitor Errors', 'Home Errors']
        #Create a variable to hold the training data
        training_data = []
        #Create a for loop to iterate through the csv file
        for row in csv_file:
            #Create a variable to hold the row of data
            row = [row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8]]
            #Append the row of data to the training data
            training_data.append(row)
        #Create a variable to hold the training data
        training_data = pd.read_csv('GameData.csv')
        #Create a variable to hold the training data
        training_data = np.array(training_data)
        #Return the training data
        return training_data
    #Create a function to clean the data
    def clean_data(self):
        #Create a variable to hold the training data
        training_data = self.retrieve_data()
        #Create a variable to hold the cleaned data
        cleaned_data = []
        #Create a for loop to iterate through the training data
        for row in training_data:
            #Create a variable to hold the date
            date = row[0]
            #Create a variable to hold the visitor team
            visitor = row[1]
            #Create a variable to hold the visitor runs
            visitor_runs = row[2]
            #Create a variable to hold the home team
            home = row[3]
            #Create a variable to hold the home runs
            home_runs = row[4]
            #Create a variable to hold the visitor hits
            visitor_hits = row[5]
            #Create a variable to hold the home hits
            home_hits = row[6]
            #Create a variable to hold the visitor errors
            visitor_errors = row[7]
            #Create a variable to hold the home errors
            home_errors = row[8]
            #Create a variable to hold the row of data
            row = [date, visitor, visitor_runs, home, home_runs, visitor_hits, home_hits, visitor_errors, home_errors]
            #Append the row of data to the cleaned data
            cleaned_data.append(row)
        #Create a variable to hold the cleaned data
        cleaned_data = pd.read_csv('GameData.csv')
        #Create a variable to hold the cleaned data
        cleaned_data = np.array(cleaned_data)
        #Return the cleaned data
        return cleaned_data
    #Create a function to create a pandas dataframe from the cleaned data
    def create_dataframe(self):
        #Create a variable to hold the cleaned data
        cleaned_data = self.clean_data()
        #Create a variable to hold the dataframe
        dataframe = pd.DataFrame(cleaned_data)
        #Return the dataframe
        return dataframe
    #Create a function to create a reinforcement learner using davinci-002 as the model
    def create_reinforcement_learner(self):
        #Create a variable to hold the dataframe
        dataframe = self.create_dataframe()
        #Create a variable to hold the reinforcement learner
        reinforcement_learner = davinci(dataframe)
        #Return the reinforcement learner
        return reinforcement_learner
    #Create a function to predict the outcome of a game
    def predict_outcome(self):
        #Create a variable to hold the reinforcement learner
        reinforcement_learner = self.create_reinforcement_learner()
        #Create a variable to hold the predicted outcome
        predicted_outcome = reinforcement_learner.predict()
        #Return the predicted outcome
        return predicted_outcome
    #Create a function to store the cleaned data in a new csv file called Cleaned-Data.csv
    def store_cleaned_data(self):
        #Create a variable to hold the cleaned data
        cleaned_data = self.clean_data()
        #Create a variable to hold the csv file
        csv_file = csv.writer(open('Cleaned-Data.csv', 'w'))
        #Create a variable to hold the header row
        header_row = ['Date', 'Visitor', 'Visitor Runs', 'Home', 'Home Runs', 'Visitor Hits', 'Home Hits', 'Visitor Errors', 'Home Errors']
        #Write the header row to the csv file
        csv_file.writerow(header_row)
        #Create a for loop to iterate through the cleaned data
        for row in cleaned_data:
            #Write the row of data to the csv file
            csv_file.writerow(row)
    #Create a function to store the predicted outcome of a game in a new csv file called Predicted-Outcome.csv
    def store_predicted_outcome(self):
        #Create a variable to hold the predicted outcome
        predicted_outcome = self.predict_outcome()
        #Create a variable to hold the csv file
        csv_file = csv.writer(open('Predicted-Outcome.csv', 'w'))
        #Create a variable to hold the header row
        header_row = ['Date', 'Visitor', 'Visitor Runs', 'Home', 'Home Runs', 'Visitor Hits', 'Home Hits', 'Visitor Errors', 'Home Errors']
        #Write the header row to the csv file
        csv_file.writerow(header_row)
        #Write the predicted outcome to the csv file
        csv_file.writerow(predicted_outcome)
#Create a variable to hold the reinforcement learner
    reinforcement_learner = ReinforcementLearner()
#Call the store cleaned data function
    reinforcement_learner.store_cleaned_data()
#Call the store predicted outcome function
    reinforcement_learner.store_predicted_outcome()

#Define the reward function using the predicted outcome in predicted-outcome.csv and the actual outcome in outcome.csv as the parameters where if the predicted outcome is correct, the reward is 1 and if the predicted outcome is incorrect, the reward is -1
    def reward_function(predicted_outcome, actual_outcome):
    #Create a variable to hold the predicted outcome
        predicted_outcome = predicted_outcome
    #Create a variable to hold the actual outcome
        actual_outcome = actual_outcome
    #Create a variable to hold the reward
        reward = 0
    #Create an if statement to check if the predicted outcome is equal to the actual outcome
        if predicted_outcome == actual_outcome:
        #Set the reward to 1
            reward = 1
    #Create an else statement to check if the predicted outcome is not equal to the actual outcome
        else:
        #Set the reward to -1
            reward = -1
    #Return the reward
        return reward

#Pass the reward function to the reinforcement learner
    reinforcement_learner.reward_function = reward_function

