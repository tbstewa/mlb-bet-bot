#Import necessary libraries
import pychadwick
import csv

#Create a python class to retrieve data from pychadwick and save it to a csv file
class RetrieveData:
    #Create a function to retrieve data from pychadwick and save it to a csv file
    def retrieve_data():
        #Create a variable to hold the davinci-002 model
        model = pychadwick.Davinci()
        #Create a variable to hold the data retrieved from Retreive-Data.py
        data = model.games()
        #Create a variable to hold the game data
        game_data = data.games()
        #Create a variable to hold the csv file
        csv_file = csv.writer(open('GameData.csv', 'w'))
        #Create a variable to hold the header row
        header_row = ['Date', 'Visitor', 'Visitor Runs', 'Home', 'Home Runs', 'Visitor Hits', 'Home Hits', 'Visitor Errors', 'Home Errors']
        #Write the header row to the csv file
        csv_file.writerow(header_row)
        #Create a for loop to iterate through the game data
        for game in game_data:
            #Create a variable to hold the date
            date = game.date
            #Create a variable to hold the visitor team
            visitor = game.visitor
            #Create a variable to hold the visitor runs
            visitor_runs = game.visitor_runs
            #Create a variable to hold the home team
            home = game.home
            #Create a variable to hold the home runs
            home_runs = game.home_runs
            #Create a variable to hold the visitor hits
            visitor_hits = game.visitor_hits
            #Create a variable to hold the home hits
            home_hits = game.home_hits
            #Create a variable to hold the visitor errors
            visitor_errors = game.visitor_errors
            #Create a variable to hold the home errors
            home_errors = game.home_errors
            #Create a variable to hold the row of data
            row = [date, visitor, visitor_runs, home, home_runs, visitor_hits, home_hits, visitor_errors, home_errors]
            #Write the row of data to the csv file
            csv_file.writerow(row)
        #Create a variable to hold the training data
        training_data = []
        #Create a variable to hold the training data
        training_data.append(row)
        #Create a variable to hold the training data
        training_data = pd.read_csv('GameData.csv')
        #Create a variable to hold the training data
        training_data = np.array(training_data)
        #Return the training data
        return training_data
    
#Create a variable to hold the data retrieved from Retreive-Data.py
data = RetrieveData.retrieve_data()
#Print the data retrieved from Retreive-Data.py
print(data)

