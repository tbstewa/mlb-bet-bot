#import modules
import time
import pandas as pd
import numpy as np
import csv
import pickle
import os
import datetime
import requests
import json
import flask
from flask import Flask, render_template, request
from flask_table import Table, Col
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from bs4 import BeautifulSoup
from TodaysGames import TodaysGames
from ReinforcementLearner import ReinforcementLearner

#This is the main class that will be used to run the program
class Core:
    #Define a function that will make a sleep loop to run the other classes every 15 minutes.
    def run(self):
        #load the other classes
        todays_games = TodaysGames()
        reinforcement_learner = ReinforcementLearner()
        #Create a while loop to run the program every 15 minutes
        while True:
            #Run the other classes
            todays_games.get_games()
            reinforcement_learner.store_cleaned_data()
            reinforcement_learner.store_outcomes()
            reinforcement_learner.predict_outcome()
            #Make a sleep loop
            time.sleep(900)

    #Define a function that will iterate through all of today's matchups and pass them to the reinforcement learner to create a prediction for each matchup.
    def make_predictions(self):
        #Create a variable to hold the matchups
        matchups = todays_games.get_matchups()
        #Create a variable to hold the predictions
        predictions = []
        #Create a for loop to iterate through the matchups
        for matchup in matchups:
            #Create a variable to hold the prediction
            prediction = reinforcement_learner.predict_outcome(matchup)
            #Append the prediction to the predictions list
            predictions.append(prediction)
        #Return the predictions
        return predictions
    
    #Define a function to create a webpage that will display the predictions for today's games using a dark theme, sortable table columns, and a table header accessible by index.html, port 8020, and that stores the html in this function and runs the flask app.
    def create_webpage(self):
        #Create a variable to hold the predictions
        predictions = self.make_predictions()
        #Create a variable to hold the matchups
        matchups = todays_games.get_matchups()
        #Create a variable to hold the html
        html = '''
        <html>
            <head>
                <title>MLB Predictions</title>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
                <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
                <style>
                    body {
                        background-color: #212121;
                        color: #fff;
                    }
                    table {
                        background-color: #424242;
                        color: #fff;
                    }
                    thead {
                        background-color: #212121;
                        color: #fff;
                    }
                    tr:nth-child(even) {
                        background-color: #424242;
                        color: #fff;
                    }
                    tr:nth-child(odd) {
                        background-color: #212121;
                        color: #fff;
                    }
                </style>
            </head>
            <body>
                <table>
                    <thead>
                        <tr>
                            <th>Matchup</th>
                            <th>Prediction</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>''' + matchups[0] + '''</td>
                            <td>''' + predictions[0] + '''</td>
                        </tr>
                        <tr>
                            <td>''' + matchups[1] + '''</td>
                            <td>''' + predictions[1] + '''</td>
                        </tr>
                        <tr>
                            <td>''' + matchups[2] + '''</td>
                            <td>''' + predictions[2] + '''</td>
                        </tr>
                        <tr>
                            <td>''' + matchups[3] + '''</td>
                            <td>''' + predictions[3] + '''</td>
                        </tr>
                        <tr>
                            <td>''' + matchups[4] + '''</td>
                            <td>''' + predictions[4] + '''</td>
                        </tr>
                        <tr>
                            <td>''' + matchups[5] + '''</td>
                            <td>''' + predictions[5] + '''</td>
                        </tr>
                        <tr>
                            <td>''' + matchups[6] + '''</td>
                            <td>''' + predictions[6] + '''</td>
                        </tr>
                        <tr>
                            <td>''' + matchups[7] + '''</td>
                            <td>''' + predictions[7] + '''</td>
                        </tr>
                        <tr>
                            <td>''' + matchups[8] + '''</td>
                            <td>''' + predictions[8] + '''</td>
                        </tr>
                        <tr>
                            <td>''' + matchups[9] + '''</td>
                            <td>''' + predictions[9] + '''</td>
                        </tr>
                        <tr>
                            <td>''' + matchups[10] + '''</td>
                            <td>''' + predictions[10] + '''</td>
                        </tr>

                    </tbody>
                </table>
            </body>
        </html>
        '''
        #Create a variable to hold the flask app
        app = Flask(__name__)
        #Create a route to display the html
        @app.route('/')
        def index():
            return html
        #Run the flask app
        app.run(port=8020)



#Create a variable to hold the core class
core = Core()
#Run the core class
core.run()
